CREATE KEYSPACE contactkeyspace WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };

USE contactkeyspace;

CREATE TABLE Contacts (
phone text,
  email text PRIMARY KEY,
 title text,
 notes text,
  firstName text,
  lastName text,
  city text,
state text,
country text
);

create table contactRelationships(
  email text ,
  contactemail text,
  contactFullName text,
  PRIMARY KEY (email,contactemail)
  );

INSERT INTO Contacts (firstName,lastName,phone,email)
  VALUES ('Erol','Akarsu','32890323', 'eakarsu@gmail.com');
INSERT INTO Contacts (firstName,lastName,phone,email)
  VALUES ('Nahit','Akarsu','4352345345', 'nakarsu@gmail.com');
INSERT INTO Contacts (firstName,lastName,phone,email)
  VALUES ('Selman','Akarsu','12333333', 'selman077@gmail.com');
INSERT INTO Contacts (firstName,lastName,phone,email)
  VALUES ('Macid','Akarsu','43534546422', 'macid@gmail.com');
INSERT INTO Contacts (firstName,lastName,phone,email)
  VALUES ('Ese','Akarsu','33334', 'ese@gmail.com');

  insert into   contactRelationships (email,contactemail,contactFullName) VALUES ('eakarsu@gmail.com','nakarsu@gmail.com','Nahit Akarsu');

  insert into   contactRelationships (email,contactemail,contactFullName) VALUES ('nakarsu@gmail.com','eakarsu@gmail.com','Erol Akarsu');
   insert into   contactRelationships (email,contactemail,contactFullName) VALUES ('ese@gmail.com','eakarsu@gmail.com','Erol Akarsu');
    insert into   contactRelationships (email,contactemail,contactFullName) VALUES ('ese@gmail.com','selman077@gmail.com','Selman Akarsu');


