"use strict";
var express = require('express');
var router = express.Router();
const cassandra = require('cassandra-driver');
const keyspace = 'contactkeyspace';
const host = 'localhost';

const allowedFields = new Set(Array.from(["phone", "email", "title", "notes", "firstname", "lastname", "city", "state", "country"]));

// middleware to use for all requests
router.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    next(); // make sure we go to the next routes and don't stop here
});


const compareField = (field, querykeyword) => {
    return field && field.match(new RegExp(querykeyword, 'g'))
}

/* GET home page. */
router.get('/contacts/search/:queryword', function (req, res) {
    const queryword = req.params.queryword;
    const client = new cassandra.Client({contactPoints: [host], keyspace: keyspace});
    client.execute('SELECT * from Contacts', [],
        (err, result) => {
            if (err) {
                res.status(404).send({msg: 'Contact not found'});
            } else {

                let newList = result.rows.filter(function (contact) {
                    var flag =
                        (compareField(contact.firstname, queryword) ||
                        compareField(contact.lastname, queryword) ||
                        compareField(contact.title, queryword) ||
                        compareField(contact.notes, queryword) ||
                        compareField(contact.email, queryword));
                    return flag;
                });
                res.json(newList);
            }
        });

});


router.delete('/contacts/delete/:email', function (req, res) {
    const email = req.params.email;
    const client = new cassandra.Client({contactPoints: [host], keyspace: keyspace});
    client.execute('delete from contacts where email = ? ', [email],
        (err) => {
            if (err) {
                res.status(404).send({msg: 'Contact not found'});
            } else {
                client.execute('delete from contactrelationships where email = ? ', [email],
                    (err) => {
                        if (err) {
                            res.status(404).send({msg: 'Contact not found'});
                        } else {
                            client.execute('select * from contactrelationships', [],
                                (err, result) => {
                                    if (err) {
                                        res.json({result: "false"});
                                    } else {
                                        //look for every record and if either email or contactemail matches to email parameter, then delete that row
                                        result.rows.map(
                                            (row) => {
                                                if (row.email == email) {
                                                    client.execute('delete from contactrelationships where email = ? AND contactemail = ?', [email, row.contactemail],
                                                        (err) => {
                                                            if (err) {
                                                                res.json({result: "false"});
                                                            }
                                                        });
                                                } else if (row.contactemail == email) {
                                                    client.execute('delete from contactrelationships where email = ? AND contactemail = ?', [row.email, email],
                                                        (err) => {
                                                            if (err) {
                                                                res.json({result: "false"});
                                                            }
                                                        });
                                                }
                                            }
                                        )
                                        res.json({result: "ok"});
                                    }
                                });


                        }
                    });

            }
        });

});


router.delete('/contacts/deleteAssociated/:personemail/:contactemail', function (req, res) {

    const personemail = req.params.personemail;
    const contactemail = req.params.contactemail;
    const client = new cassandra.Client({contactPoints: [host], keyspace: keyspace});
    client.execute('delete from contactrelationships where email = ? and contactemail = ? ', [personemail, contactemail],
        (err) => {

            if (err) {
                res.status(404).send({msg: 'Contact not found'});
            } else {
                getContactRecord(req, res, personemail);
            }
        });

});

router.put('/contacts/addAssociated/:personemail/:contactemail/:fullname', function (req, res) {

    const personemail = req.params.personemail;
    const contactemail = req.params.contactemail;
    const fullname = req.params.fullname;

    const client = new cassandra.Client({contactPoints: [host], keyspace: keyspace});
    client.execute('insert into contactrelationships (email,contactemail,contactfullname) VALUES(?,?,?) ', [personemail, contactemail, fullname],
        (err) => {

            if (err) {
                res.status(404).send({msg: 'Contact not found'});
            } else {
                getContactRecord(req, res, personemail);
            }
        });

});

function getContactRecord(req, res, email) {
    const client = new cassandra.Client({contactPoints: [host], keyspace: keyspace});
    client.execute('select * from contacts where email = ? ', [email],
        (err, result) => {

            if (err) {
                res.status(404).send({msg: 'Contact not found'});
            } else {
                client.execute('select * from contactrelationships where email = ? ', [email],
                    (err, relatedContacts) => {
                        if (err) {
                            res.status(404).send({msg: 'Contact relations found'});
                        } else {
                            client.execute('select email,firstname,lastname from contacts', [],
                                (err, potentialContacts) => {
                                    if (err) {
                                        res.status(404).send({msg: 'All Contacts not found'});
                                    } else {
                                        potentialContacts.rows = potentialContacts.rows.filter((contact) => {
                                            const isFound = relatedContacts.rows.some(nextc => {
                                                return nextc.contactemail == contact.email;
                                            })
                                            return !isFound && email != contact.email;
                                        })
                                        result.rows[0]["relatedcontacts"] = relatedContacts.rows;
                                        result.rows[0]["potentialcontacts"] = potentialContacts.rows;
                                        res.json(result.rows);
                                    }
                                });
                        }
                    });
            }

        }
    );
}
router.get('/contacts/edit/:email', function (req, res) {
    const email = req.params.email;
    getContactRecord(req, res, email);
});

router.post('/contacts/add', function (req, res) {

    const reqStr = JSON.stringify(req.body);
    const query = `insert into contacts  JSON '${reqStr}'`;

    const client = new cassandra.Client({contactPoints: [host], keyspace: keyspace});
    client.execute(query, [],
        (err) => {
            if (err) {
                res.status(404).send({msg: err});
            } else {
                res.json(req.body);
            }
        });
});

router.post('/contacts/update', function (req, res) {

    const pairs = new Map();
    for (var key in req.body) {
        const elemVal = req.body[key];
        if (key != 'email' && allowedFields.has(key))
            pairs.set(key, elemVal);
    }

    const setKeys = Array.from(pairs.keys()).map((key)=>`${key}=?`).join(",");
    const setValues = Array.from(pairs.values());
    if (setValues.length == 0) {
        res.json(req.body);
        return;
    }

    const query = `update contacts  set ${setKeys} where email=?`;
    setValues.push(req.body['email']);

    const client = new cassandra.Client({contactPoints: [host], keyspace: keyspace});
    client.execute(query, setValues,
        (err) => {
            if (err) {
                res.status(404).send({msg: err});
            } else {
                res.json(req.body);
            }
        });
});

module.exports = router;
